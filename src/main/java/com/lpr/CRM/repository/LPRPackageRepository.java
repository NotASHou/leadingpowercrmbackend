package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.lprpackage.LPRPackage;

public interface LPRPackageRepository extends JpaRepository<LPRPackage, Long> {

}
