package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.LeadSource;

public interface LeadSourceRepository extends JpaRepository<LeadSource, Long> {

}
