package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Candidate;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

}
