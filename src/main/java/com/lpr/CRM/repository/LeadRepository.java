package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Lead;

public interface LeadRepository extends JpaRepository<Lead, Long> {

}
