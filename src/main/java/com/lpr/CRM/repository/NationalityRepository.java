package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Nationality;

public interface NationalityRepository extends JpaRepository<Nationality, Long> {

}
