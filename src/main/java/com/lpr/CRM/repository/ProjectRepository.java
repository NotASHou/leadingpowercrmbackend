package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Long> {

}
