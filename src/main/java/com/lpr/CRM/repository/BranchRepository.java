package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Branch;

public interface BranchRepository extends JpaRepository<Branch, Long> {
	
}
