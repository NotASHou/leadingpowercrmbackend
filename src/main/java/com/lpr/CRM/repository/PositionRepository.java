package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {

}
