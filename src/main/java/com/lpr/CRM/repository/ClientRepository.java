package com.lpr.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
