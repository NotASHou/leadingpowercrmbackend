package com.lpr.CRM.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lpr.CRM.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByUsername(final String username);
}
