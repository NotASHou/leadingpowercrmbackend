package com.lpr.CRM.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.lpr.CRM.entity.Account;
import com.lpr.CRM.entity.Role;
import com.lpr.CRM.repository.AccountRepository;
import com.lpr.CRM.repository.RoleRepository;

@Service
public class MySecurityService {
	
	Logger logger = LoggerFactory.getLogger(MySecurityService.class);
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private AccountRepository accountRepository;
	
	public Role currentRole() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String role = authentication.getAuthorities().stream()
			     .map(r -> r.getAuthority()).findFirst().get();
		
		Role roleObj = roleRepository.findByName(role).get(0);
		return roleObj;
	}
	
	public Account currentAccount() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
		
		return accountRepository.findByUsername(name).get();
	}
	
    public boolean isAdmin() {
    	return currentRole().getLevel() < 2;
    }

}
