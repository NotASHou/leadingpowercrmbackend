package com.lpr.CRM.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.entity.Lead;
import com.lpr.CRM.model.MyInterviewDate;
import com.lpr.CRM.model.LeadStatus;
import com.lpr.CRM.repository.CandidateRepository;

@Service
public class LeadService {
	@Autowired
	MySecurityService mySecurityService;
	@Autowired
	CandidateRepository candidateRepository;
	
	public List<MyInterviewDate> top5thLead(List<Lead> list) {
		list.sort((l1, l2) -> {
    		return l1.getDatetime().compareTo(l2.getDatetime());
    	});
    	List<String> top5thDate = new ArrayList<String>();
    	List<MyInterviewDate> top5thLead = new ArrayList<MyInterviewDate>();
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	list.forEach(lead -> {
    		String dateStr = sdf.format(lead.getDatetime());
    		
    		MyInterviewDate cur;
    		if(!top5thDate.contains(dateStr)) {
    			top5thDate.add(dateStr);
    			if(top5thDate.size() > 5) {
        			return;
        		}
    			cur = new MyInterviewDate();
    			cur.setDate(lead.getDatetime());
    			top5thLead.add(cur);
    		} else {
    			cur = top5thLead.get(top5thLead.size()-1);
    		}
    		
    		cur.getList().add(lead);
    	});
    	
    	return top5thLead;
	}
	
    public Candidate createCandidateForLead(Lead lead) {
		if(lead.getCandidate() == null) {
			Candidate candidate = new Candidate();
			lead.setCandidate(candidate);
			candidate.setLead(lead);
			candidate.setOwner(mySecurityService.currentAccount());
			candidate.setRegisterDate(new Date());
			candidateRepository.save(candidate);
			
			return candidate;
		} else {
			return lead.getCandidate();
		}
    }

    public boolean leadIsAppoint(Lead x) {
    	Date today = new Date();
    	return x.getStatus()!=LeadStatus.appoint || x.getDatetime()==null || x.getDatetime().before(today);
    }
        
    public boolean isOwner(Lead x) {
    	return x.getAddby().getId() == mySecurityService.currentAccount().getId();
    }
}
