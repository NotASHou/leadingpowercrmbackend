package com.lpr.CRM.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.model.ClientInterviewDate;

@Service
public class CandidateService {
	public List<ClientInterviewDate> top5thCandidate(List<Candidate> list) {
		list.sort((c1, c2) -> {
			return c1.getDate().compareTo(c2.getDate());
		});
		List<String> top5thDate = new ArrayList<String>();
		List<ClientInterviewDate> top5th = new ArrayList<ClientInterviewDate>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		list.forEach(candidate -> {
    		String dateStr = sdf.format(candidate.getDate());
    		
    		ClientInterviewDate cur;
    		if(!top5thDate.contains(dateStr)) {
    			top5thDate.add(dateStr);
    			if(top5thDate.size() > 5) {
        			return;
        		}
    			cur = new ClientInterviewDate();
    			cur.setDate(candidate.getDate());
    			top5th.add(cur);
    		} else {
    			cur = top5th.get(top5th.size()-1);
    		}
    		
    		cur.getList().add(candidate);
		});
		
		return top5th;
	}
}
