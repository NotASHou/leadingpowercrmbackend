package com.lpr.CRM.model;

public enum LeadStatus {
	noaction,
	call,
	conversation,
	interest,
	appoint,
	pass
}
