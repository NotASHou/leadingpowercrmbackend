package com.lpr.CRM.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lpr.CRM.entity.Candidate;

import lombok.Getter;
import lombok.Setter;

public class ClientInterviewDate {
	@Getter @Setter Date date;
	@Getter @Setter List<Candidate> list = new ArrayList<Candidate>();
}
