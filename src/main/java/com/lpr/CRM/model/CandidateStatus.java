package com.lpr.CRM.model;

public enum CandidateStatus {
	idle,
	pass,
	clientinterview,
	firstday,
	trainingcomplete,
	garanteeneed,
	reject,
	noshow,
	dropout,
	terminate
}
