package com.lpr.CRM.model;

import java.util.Set;

import com.lpr.CRM.entity.Account;
import com.lpr.CRM.entity.Role;

import lombok.Getter;
import lombok.Setter;

public class AccountNoPassword {
	@Getter @Setter private Long id;

	@Getter @Setter private String username;

	@Getter @Setter private Set<Role> roles;

	@Getter @Setter private boolean enabled = true;
    
	@Getter @Setter private String firstname;
    
	@Getter @Setter private String lastname;
        
	@Getter @Setter private Long reserved;

    public AccountNoPassword(Account acc) {
    	this.id = acc.getId();
    	this.username = acc.getUsername();
    	this.roles = acc.getRoles();
    	this.enabled = acc.isEnabled();
    	this.firstname = acc.getFirstname();
    	this.lastname = acc.getLastname();
    	this.reserved = acc.getReserved();
    }
}
