package com.lpr.CRM.model;

import lombok.Getter;
import lombok.Setter;

public class GeneralResponse {
	@Getter @Setter private int code = 0;
	@Getter @Setter private String message = "success";
	
	public GeneralResponse() {}
	
	public GeneralResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}
}
