package com.lpr.CRM.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lpr.CRM.entity.Lead;

import lombok.Getter;
import lombok.Setter;

public class MyInterviewDate {
	@Getter @Setter Date date;
	@Getter @Setter List<Lead> list = new ArrayList<Lead>();
}
