package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedPackage {
	@Getter @Setter private String name;
	@Getter @Setter private String data;
}
