package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class UpdatedProject extends AddedProject {
	@Getter @Setter private Long id;
}
