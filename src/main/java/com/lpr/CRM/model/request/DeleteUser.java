package com.lpr.CRM.model.request;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class DeleteUser {
	@Getter @Setter private List<Long> ids;
}
