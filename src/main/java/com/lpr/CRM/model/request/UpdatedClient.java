package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class UpdatedClient extends AddedClient {
	@Getter @Setter private Long id;
}
