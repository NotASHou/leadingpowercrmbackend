package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class UpdatedPackage {
	@Getter @Setter Long id;
	@Getter @Setter String name;
	@Getter @Setter String data;
}
