package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class UpdatedPosition extends AddedPosition {
	@Getter @Setter private Long id;
}
