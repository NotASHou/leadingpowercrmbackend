package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedUser {
	@Getter @Setter private String username;
	@Getter @Setter private String password;
	@Getter @Setter private String firstname;
	@Getter @Setter private String lastname;
	@Getter @Setter private Integer roleId;
}
