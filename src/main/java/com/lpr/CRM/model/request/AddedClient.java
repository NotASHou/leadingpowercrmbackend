package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedClient {
	@Getter @Setter private String name;
}
