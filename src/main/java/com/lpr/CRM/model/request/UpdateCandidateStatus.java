package com.lpr.CRM.model.request;

import java.util.Date;

import com.lpr.CRM.model.CandidateStatus;

import lombok.Getter;
import lombok.Setter;

public class UpdateCandidateStatus {
	@Getter @Setter Long candidateId;
	@Getter @Setter CandidateStatus status;
	@Getter @Setter Date date;
	@Getter @Setter Long clientId;
	@Getter @Setter Long branchId;
	@Getter @Setter Long projectId;
	@Getter @Setter Long positionId;
	@Getter @Setter String note;
	@Getter @Setter Double salary;
}
