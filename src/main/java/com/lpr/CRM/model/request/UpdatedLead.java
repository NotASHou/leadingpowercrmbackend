package com.lpr.CRM.model.request;

import java.util.Date;

import com.lpr.CRM.model.LeadStatus;

import lombok.Getter;
import lombok.Setter;

public class UpdatedLead {
	@Getter @Setter Long id;
	@Getter @Setter String name;
	@Getter @Setter String surname;
	@Getter @Setter String nickname;
	@Getter @Setter String mobile;
	@Getter @Setter Long currentSourceId;
	@Getter @Setter LeadStatus currentStatus;
	@Getter @Setter Date datetime;
}
