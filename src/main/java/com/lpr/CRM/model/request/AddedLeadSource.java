package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedLeadSource {
	@Getter @Setter private String name;
	@Getter @Setter private Integer cost;
}
