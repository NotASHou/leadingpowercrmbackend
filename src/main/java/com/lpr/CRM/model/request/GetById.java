package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class GetById {
	@Getter @Setter Long id;
}
