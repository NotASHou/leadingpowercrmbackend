package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedBranch {
	@Getter @Setter private String name;
	@Getter @Setter private Long clientId;
}
