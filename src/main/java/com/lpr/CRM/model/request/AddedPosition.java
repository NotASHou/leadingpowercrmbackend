package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedPosition {
	@Getter @Setter private String name;
	@Getter @Setter private Long projectId;
	@Getter @Setter private Long packageId;
}
