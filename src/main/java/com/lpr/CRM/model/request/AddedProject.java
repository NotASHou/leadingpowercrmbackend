package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedProject {
	@Getter @Setter private String name;
	@Getter @Setter private Long branchId;
}
