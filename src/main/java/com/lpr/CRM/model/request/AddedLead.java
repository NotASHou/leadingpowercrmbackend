package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class AddedLead {
    @Getter @Setter private String name;
    @Getter @Setter private String surname;
	@Getter @Setter private String nickname;
	@Getter @Setter private String mobile;
	@Getter @Setter private Long leadSourceId;
}
