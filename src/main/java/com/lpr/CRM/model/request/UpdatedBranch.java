package com.lpr.CRM.model.request;

import lombok.Getter;
import lombok.Setter;

public class UpdatedBranch extends AddedBranch {
	@Getter @Setter private Long id;
}
