package com.lpr.CRM.model.request;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class UpdateCandidateDetail {
	@Getter @Setter Long candidateId;
	
	@Getter @Setter String name;
	@Getter @Setter String surname;
	@Getter @Setter String nickname;
	@Getter @Setter String mobile;
	@Getter @Setter Date dateOfBirth;
	@Getter @Setter Long nationalityId;
}
