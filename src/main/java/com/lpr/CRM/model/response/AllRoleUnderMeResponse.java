package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Role;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class AllRoleUnderMeResponse extends GeneralResponse {
	@Getter @Setter private List<Role> roles;
}
