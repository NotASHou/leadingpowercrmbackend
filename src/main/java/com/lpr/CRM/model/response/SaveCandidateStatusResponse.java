package com.lpr.CRM.model.response;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class SaveCandidateStatusResponse extends GeneralResponse {
	@Getter @Setter Candidate candidate;

}
