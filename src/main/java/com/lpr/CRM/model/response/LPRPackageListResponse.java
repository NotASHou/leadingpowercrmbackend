package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.lprpackage.LPRPackage;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class LPRPackageListResponse extends GeneralResponse {
	@Getter @Setter List<LPRPackage> list;
}
