package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.model.GeneralResponse;
import com.lpr.CRM.model.MyInterviewDate;

import lombok.Getter;
import lombok.Setter;

public class MyInterviewResponse extends GeneralResponse {
	@Getter @Setter List<MyInterviewDate> list;
}
