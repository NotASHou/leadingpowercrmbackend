package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class CandidateListResponse extends GeneralResponse {
	@Getter @Setter private List<Candidate> candidateList;
}
