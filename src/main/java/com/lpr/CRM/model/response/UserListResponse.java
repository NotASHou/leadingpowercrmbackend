package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Account;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class UserListResponse extends GeneralResponse {
	@Getter @Setter List<Account> accounts;
}
