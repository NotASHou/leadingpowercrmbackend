package com.lpr.CRM.model.response;

import com.lpr.CRM.entity.Lead;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class LeadByIdResponse extends GeneralResponse {
	@Getter @Setter Lead lead;
}
