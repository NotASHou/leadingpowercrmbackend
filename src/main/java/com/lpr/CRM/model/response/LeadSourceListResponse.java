package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.LeadSource;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class LeadSourceListResponse extends GeneralResponse {
	@Getter @Setter private List<LeadSource> leadsourcelist;
}
