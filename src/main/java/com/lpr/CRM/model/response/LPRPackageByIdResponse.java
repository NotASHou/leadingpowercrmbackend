package com.lpr.CRM.model.response;

import com.lpr.CRM.entity.lprpackage.LPRPackage;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class LPRPackageByIdResponse extends GeneralResponse {
	@Getter @Setter private LPRPackage lprpackage;
}
