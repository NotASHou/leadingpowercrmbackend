package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Position;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class PositionListResponse extends GeneralResponse {
	@Getter @Setter private List<Position> positions;
}
