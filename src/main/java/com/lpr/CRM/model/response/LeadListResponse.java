package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Lead;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class LeadListResponse extends GeneralResponse {
	@Getter @Setter private List<Lead> leadlist;
}
