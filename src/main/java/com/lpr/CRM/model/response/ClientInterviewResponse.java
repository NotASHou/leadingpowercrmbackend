package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.model.ClientInterviewDate;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class ClientInterviewResponse extends GeneralResponse {
	@Getter @Setter List<ClientInterviewDate> list;
}
