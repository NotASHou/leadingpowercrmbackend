package com.lpr.CRM.model.response;

import java.util.ArrayList;
import java.util.List;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class InCompleteResponse extends GeneralResponse {
	@Getter @Setter List<Candidate> list = new ArrayList<Candidate>();
}
