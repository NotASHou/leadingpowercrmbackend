package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Client;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class ClientListResponse extends GeneralResponse {
	@Getter @Setter private List<Client> clients;
}
