package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Nationality;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class AllNationalityResponse extends GeneralResponse {
	@Getter @Setter List<Nationality> nationalities;
}
