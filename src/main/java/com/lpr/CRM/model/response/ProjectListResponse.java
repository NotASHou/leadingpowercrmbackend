package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Project;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class ProjectListResponse extends GeneralResponse {
	@Getter @Setter private List<Project> projects;
}
