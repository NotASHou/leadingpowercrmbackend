package com.lpr.CRM.model.response;

import lombok.Getter;
import lombok.Setter;

public class MyRoleResponse {
	@Getter @Setter private String role;
}
