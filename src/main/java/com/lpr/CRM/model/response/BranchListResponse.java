package com.lpr.CRM.model.response;

import java.util.List;

import com.lpr.CRM.entity.Branch;
import com.lpr.CRM.model.GeneralResponse;

import lombok.Getter;
import lombok.Setter;

public class BranchListResponse extends GeneralResponse {
	@Getter @Setter private List<Branch> branches;
}
