package com.lpr.CRM.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.lpr.CRM.entity.lprpackage.LPRPackage;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_generator")
    @SequenceGenerator(name="position_generator", sequenceName = "position_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Getter @Setter private String name;
	@ManyToOne
	@Getter @Setter private LPRPackage lprPackage;
}
