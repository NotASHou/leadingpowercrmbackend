package com.lpr.CRM.entity.lprpackage;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.lpr.CRM.model.PackageType;

import lombok.Getter;
import lombok.Setter;

@Entity
public class LPRPackage {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_generator")
    @SequenceGenerator(name="package_generator", sequenceName = "package_seq", allocationSize=50)
    @Getter @Setter private Long id;
	
	@Enumerated(EnumType.STRING)
	@Getter @Setter private PackageType type;
	
    @Getter @Setter private String name;
    @Getter @Setter private String data;

}
