package com.lpr.CRM.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Nationality {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nationality_generator")
    @SequenceGenerator(name="nationality_generator", sequenceName = "nationality_seq", allocationSize=50)
    @Getter @Setter private Long id;
	
	@Getter @Setter private String name;
}
