package com.lpr.CRM.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.lpr.CRM.model.CandidateStatus;
import com.lpr.CRM.service.MySecurityService;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Candidate {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidate_generator")
    @SequenceGenerator(name="candidate_generator", sequenceName = "candidate_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @OneToOne
    @Getter @Setter private Lead lead;
    @Getter @Setter private Date registerDate;
    @ManyToOne
    @Getter @Setter private Account owner;
    @Enumerated(EnumType.STRING)
    @Getter @Setter private CandidateStatus status;
    @Getter @Setter private Date date;
    @ManyToOne
    @Getter @Setter private Client client;
    @ManyToOne
    @Getter @Setter private Branch branch;
    @ManyToOne
    @Getter @Setter private Project project;
    @ManyToOne
    @Getter @Setter private Position position;
    @Getter @Setter private Double salary;
    @Getter @Setter private String note;
    @ManyToOne
    @Getter @Setter private Nationality nationality;
    @Getter @Setter private Date dateOfBirth;
    
    public boolean isAppoint() {
    	Date today = new Date();
    	return getDate()!=null && getDate().after(today) && getStatus()==CandidateStatus.clientinterview;
    }
    
    public boolean isComplete() {
    	return status == CandidateStatus.idle ||
    			status == CandidateStatus.pass ||
    			status == CandidateStatus.clientinterview ||
    			status == CandidateStatus.firstday;
    }
    
    public boolean isOwner(MySecurityService mySecurityService) {
    	return getOwner().getId() == mySecurityService.currentAccount().getId();
    }
}
