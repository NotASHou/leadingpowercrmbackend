package com.lpr.CRM.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
public class LeadSource {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "leadsource_generator")
    @SequenceGenerator(name="leadsource_generator", sequenceName = "leadsource_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Getter @Setter private String name;
    @Getter @Setter private Integer cost;
    @Getter @Setter private Date createdate;
}
