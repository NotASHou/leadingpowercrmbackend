package com.lpr.CRM.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.lpr.CRM.model.LeadStatus;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Lead {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lead_generator")
    @SequenceGenerator(name="lead_generator", sequenceName = "lead_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Getter @Setter private String name;
    @Getter @Setter private String surname;
    @Getter @Setter private String nickname;
    @Getter @Setter private String mobile;
    @Enumerated(EnumType.STRING)
    @Getter @Setter private LeadStatus status = LeadStatus.noaction;
    @Getter @Setter private Date datetime;
    
    @ManyToOne
    @Getter @Setter private LeadSource source;
    
    @ManyToOne
    @Getter @Setter private Account addby;
    
    @OneToOne
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @Getter @Setter private Candidate candidate = null;
    
    @Getter @Setter private boolean enabled = true;
}
