package com.lpr.CRM.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_generator")
    @SequenceGenerator(name="project_generator", sequenceName = "project_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Getter @Setter private String name;
    
    @OneToMany(fetch = FetchType.LAZY)
    @Getter @Setter private List<Position> positions;
}
