package com.lpr.CRM.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_generator")
    @SequenceGenerator(name="account_generator", sequenceName = "account_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Column(unique = true)
    @Getter @Setter private String username;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Getter @Setter private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @Getter @Setter private Set<Role> roles;

    @Getter @Setter private boolean enabled = true;
    
    @Getter @Setter private String firstname;
    
    @Getter @Setter private String lastname;
        
    @Getter @Setter private Long reserved;
    
    public Account() {
    	roles = new HashSet<Role>();
    }

}
