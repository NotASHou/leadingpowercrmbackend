package com.lpr.CRM.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Client {
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
    @SequenceGenerator(name="client_generator", sequenceName = "client_seq", allocationSize=50)
    @Getter @Setter private Long id;

    @Getter @Setter private String name;
    
    @OneToMany(fetch = FetchType.LAZY)
    @Getter @Setter private List<Branch> branches = new ArrayList<>();
}
