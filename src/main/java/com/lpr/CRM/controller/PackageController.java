package com.lpr.CRM.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.lprpackage.LPRPackage;
import com.lpr.CRM.model.GeneralResponse;
import com.lpr.CRM.model.PackageType;
import com.lpr.CRM.model.request.AddedPackage;
import com.lpr.CRM.model.request.GetById;
import com.lpr.CRM.model.request.UpdatedPackage;
import com.lpr.CRM.model.response.LPRPackageByIdResponse;
import com.lpr.CRM.model.response.LPRPackageListResponse;
import com.lpr.CRM.repository.LPRPackageRepository;

@RestController
public class PackageController {

	@Autowired
	LPRPackageRepository lprPackageRepository;
	
	@PostMapping("flatpackage/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public LPRPackageListResponse listFlatPackage() {
		
		List<LPRPackage> list = lprPackageRepository.findAll().stream().filter(x -> x.getType() == PackageType.flat).collect(Collectors.toList());
		
		LPRPackageListResponse r = new LPRPackageListResponse();
		r.setList(list);
		
		return r;
	}
	
	@PostMapping("flatpackage/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse addFlatPackage(@RequestBody AddedPackage addP) {
		LPRPackage f = new LPRPackage();
		f.setName(addP.getName());
		f.setData(addP.getData());
		f.setType(PackageType.flat);
		
		lprPackageRepository.save(f);
		
		return new GeneralResponse();
	}
	
	@PostMapping("flatpackage/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse updateFlatPackage(@RequestBody UpdatedPackage updateP) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(updateP.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			f.setName(updateP.getName());
			f.setData(updateP.getData());
			lprPackageRepository.save(f);
			
			return new GeneralResponse();
		} else {
			return new GeneralResponse(-1, "Package id " + updateP.getId() + " not found.");
		}
	}
	
	@PostMapping("flatpackage/getbyid")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse flatPackageById(@RequestBody GetById request) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(request.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			LPRPackageByIdResponse r = new LPRPackageByIdResponse();
			r.setLprpackage(f);
			return r;
		} else {
			return new GeneralResponse(-1, "Package id " + request.getId() + " not found.");
		}
	}
	
	@PostMapping("percentpackage/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public LPRPackageListResponse listPercentPackage() {
		
		List<LPRPackage> list = lprPackageRepository.findAll().stream().filter(x -> x.getType() == PackageType.percent).collect(Collectors.toList());
		
		LPRPackageListResponse r = new LPRPackageListResponse();
		r.setList(list);
		
		return r;
	}
	
	@PostMapping("percentpackage/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse addPercentPackage(@RequestBody AddedPackage addP) {
		LPRPackage f = new LPRPackage();
		f.setName(addP.getName());
		f.setData(addP.getData());
		f.setType(PackageType.percent);
		
		lprPackageRepository.save(f);
		
		return new GeneralResponse();
	}
	
	@PostMapping("percentpackage/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse updatePercentPackage(@RequestBody UpdatedPackage updateP) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(updateP.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			f.setName(updateP.getName());
			f.setData(updateP.getData());
			lprPackageRepository.save(f);
			
			return new GeneralResponse();
		} else {
			return new GeneralResponse(-1, "Package id " + updateP.getId() + " not found.");
		}
	}
	
	@PostMapping("percentpackage/getbyid")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse percentPackageById(@RequestBody GetById request) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(request.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			LPRPackageByIdResponse r = new LPRPackageByIdResponse();
			r.setLprpackage(f);
			return r;
		} else {
			return new GeneralResponse(-1, "Package id " + request.getId() + " not found.");
		}
	}

	@PostMapping("contractpackage/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public LPRPackageListResponse listContractPackage() {
		
		List<LPRPackage> list = lprPackageRepository.findAll().stream().filter(x -> x.getType() == PackageType.contract).collect(Collectors.toList());
		
		LPRPackageListResponse r = new LPRPackageListResponse();
		r.setList(list);
		
		return r;
	}
	
	@PostMapping("contractpackage/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse addContractPackage(@RequestBody AddedPackage addP) {
		LPRPackage f = new LPRPackage();
		f.setName(addP.getName());
		f.setData(addP.getData());
		f.setType(PackageType.contract);
		
		lprPackageRepository.save(f);
		
		return new GeneralResponse();
	}
	
	@PostMapping("contractpackage/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse updateContractPackage(@RequestBody UpdatedPackage updateP) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(updateP.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			f.setName(updateP.getName());
			f.setData(updateP.getData());
			lprPackageRepository.save(f);
			
			return new GeneralResponse();
		} else {
			return new GeneralResponse(-1, "Package id " + updateP.getId() + " not found.");
		}
	}
	
	@PostMapping("contractpackage/getbyid")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
	public GeneralResponse contractPackageById(@RequestBody GetById request) {
		Optional<LPRPackage> fOpt = lprPackageRepository.findById(request.getId());
		if(fOpt.isPresent()) {
			LPRPackage f = fOpt.get();
			LPRPackageByIdResponse r = new LPRPackageByIdResponse();
			r.setLprpackage(f);
			return r;
		} else {
			return new GeneralResponse(-1, "Package id " + request.getId() + " not found.");
		}
	}
}
