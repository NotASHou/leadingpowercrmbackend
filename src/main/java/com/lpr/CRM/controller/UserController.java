package com.lpr.CRM.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Account;
import com.lpr.CRM.entity.Role;
import com.lpr.CRM.model.GeneralResponse;
import com.lpr.CRM.model.request.AddedUser;
import com.lpr.CRM.model.request.DeleteUser;
import com.lpr.CRM.model.response.UserListResponse;
import com.lpr.CRM.repository.AccountRepository;
import com.lpr.CRM.repository.RoleRepository;
import com.lpr.CRM.service.MySecurityService;

@RestController
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	MySecurityService mySecurityService;
	
    @GetMapping("/user/list")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
    public UserListResponse userlist() {
        List<Account> accList = accountRepository.findAll();
        List<Account> resList = new ArrayList<Account>();
        accList.forEach((acc) -> {
        	if(acc.isEnabled())
        		resList.add(acc);
        });
        
        UserListResponse response = new UserListResponse();
        response.setAccounts(resList);
        
        return response;
    }
    
    @PostMapping("/user/add")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
    public GeneralResponse addUser(@RequestBody AddedUser useradded) {
    	
    	String encodedPwd = BCrypt.hashpw(useradded.getPassword(), BCrypt.gensalt());
    	
    	Account a = new Account();
    	a.setUsername(useradded.getUsername());
    	a.setPassword(encodedPwd);
    	a.setFirstname(useradded.getFirstname());
    	a.setLastname(useradded.getLastname());
    	a.setReserved(new Long(0));
    	a.setEnabled(true);
    	Role userRole = roleRepository.findById((long) useradded.getRoleId()).get();
    	
    	Set<Role> newRoles = new HashSet<Role>();
    	newRoles.add(userRole);
    	a.setRoles(newRoles);
    	accountRepository.save(a);
    	
    	return new GeneralResponse();
    }
    
    @PostMapping("/user/delete")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
    public GeneralResponse deleteUser(@RequestBody DeleteUser deleteUser) {
    	for(Long id : deleteUser.getIds()) {
    		logger.info("deleteUser with id : " + id);
        	Optional<Account> account = accountRepository.findById(id);
        	if(account.isPresent()) {
        		Role current = mySecurityService.currentRole();
            	Role deletedRole = new ArrayList<Role>(account.get().getRoles()).get(0);
            	logger.info(current.getLevel() + " > " + deletedRole.getLevel());
            	if(current.getLevel() < deletedRole.getLevel()) {
            		Account accountWillDeleted = accountRepository.findById(id).get();
            		accountWillDeleted.setEnabled(false);
            		accountRepository.save(accountWillDeleted);
            		logger.info("delete id " + id + " success");
            	} else {
            		logger.info("delete id " + id + " denied");
            	}
        	} else {
        		logger.info("delete id " + id + " not found");
        	}
    	}
    	return new GeneralResponse();
    }
}
