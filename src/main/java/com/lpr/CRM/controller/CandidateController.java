package com.lpr.CRM.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Branch;
import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.entity.Client;
import com.lpr.CRM.entity.Lead;
import com.lpr.CRM.entity.Nationality;
import com.lpr.CRM.entity.Position;
import com.lpr.CRM.entity.Project;
import com.lpr.CRM.model.ClientInterviewDate;
import com.lpr.CRM.model.request.GetById;
import com.lpr.CRM.model.request.UpdateCandidateDetail;
import com.lpr.CRM.model.request.UpdateCandidateStatus;
import com.lpr.CRM.model.response.CandidateByIdResponse;
import com.lpr.CRM.model.response.CandidateListResponse;
import com.lpr.CRM.model.response.ClientInterviewResponse;
import com.lpr.CRM.model.response.InCompleteResponse;
import com.lpr.CRM.model.response.SaveCandidateStatusResponse;
import com.lpr.CRM.repository.BranchRepository;
import com.lpr.CRM.repository.CandidateRepository;
import com.lpr.CRM.repository.ClientRepository;
import com.lpr.CRM.repository.LeadRepository;
import com.lpr.CRM.repository.NationalityRepository;
import com.lpr.CRM.repository.PositionRepository;
import com.lpr.CRM.repository.ProjectRepository;
import com.lpr.CRM.service.CandidateService;
import com.lpr.CRM.service.MySecurityService;

@RestController
public class CandidateController {
	@Autowired
	LeadRepository leadRepository;
	@Autowired
	CandidateRepository candidateRepository;
	@Autowired
	ClientRepository clientRepository;
	@Autowired
	BranchRepository branchRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	PositionRepository positionRepository;
	@Autowired
	NationalityRepository nationalityRepository;
	@Autowired
	MySecurityService mySecurityService;
	@Autowired
	CandidateService candidateService;
	
	@GetMapping("/candidate/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public CandidateListResponse candidateList() {
		CandidateListResponse response = new CandidateListResponse();
		List<Candidate> candidateList = candidateRepository.findAll().stream().filter(x -> {
			if(mySecurityService.isAdmin()) {
				return true;
			} else {
				if(x.isOwner(mySecurityService)) {
					return true;
				} else {
					return false;
				}
			}
		}).collect(Collectors.toList());;
		
		response.setCandidateList(candidateList);
		return response;
	}
	
	@PostMapping("/candidate/save/status")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public SaveCandidateStatusResponse saveCandidateStatus(@RequestBody UpdateCandidateStatus status) {
		Optional<Candidate> optC = candidateRepository.findById(status.getCandidateId());
		
		if(optC.isPresent()) {
			
			Candidate c = optC.get();
			c.setStatus(status.getStatus());
			Client client = status.getClientId()!=null ? clientRepository.findById(status.getClientId()).get() : null;
			c.setClient(client);
			Branch branch = status.getBranchId()!=null ? branchRepository.findById(status.getBranchId()).get() : null;
			c.setBranch(branch);
			Project project = status.getProjectId()!=null ? projectRepository.findById(status.getProjectId()).get() : null;
			c.setProject(project);
			Position position = status.getPositionId()!=null ? positionRepository.findById(status.getPositionId()).get() : null;
			c.setPosition(position);
			c.setDate(status.getDate());
			c.setNote(status.getNote());
			c.setSalary(status.getSalary());
			
			candidateRepository.save(c);
			
			SaveCandidateStatusResponse r = new SaveCandidateStatusResponse();
			r.setCandidate(c);
			return r;
		} else {
			SaveCandidateStatusResponse r = new SaveCandidateStatusResponse();
			r.setCode(-1);
			r.setMessage("Candidate " + status.getCandidateId() + " not found");
			return r;
		}
	}
	
	@PostMapping("/candidate/save/detail")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public SaveCandidateStatusResponse saveCandidateDetail(@RequestBody UpdateCandidateDetail detail) {
		Optional<Candidate> optC = candidateRepository.findById(detail.getCandidateId());
		
		if(optC.isPresent()) {
			Candidate c = optC.get();
			Lead lead = c.getLead();
			
			lead.setName(detail.getName());
			lead.setSurname(detail.getSurname());
			lead.setNickname(detail.getNickname());
			lead.setMobile(detail.getMobile());
			
			if(detail.getNationalityId()!=null) {
				Optional<Nationality> nOpt = nationalityRepository.findById(detail.getNationalityId());
				if(nOpt.isPresent()) {
					Nationality n = nOpt.get();
					c.setNationality(n);
				}
			}
			c.setDateOfBirth(detail.getDateOfBirth());
			
			leadRepository.save(lead);
			candidateRepository.save(c);
			SaveCandidateStatusResponse r = new SaveCandidateStatusResponse();
			r.setCandidate(c);
			return r;
		} else {
			SaveCandidateStatusResponse r = new SaveCandidateStatusResponse();
			r.setCode(-1);
			r.setMessage("Candidate " + detail.getCandidateId() + " not found");
			return r;
		}
	}
	
	@PostMapping("/candidate/clientInterview")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public ClientInterviewResponse clientInterview() {
		List<Candidate> list = candidateRepository.findAll().stream().filter(x -> {
			/*if(wait for !isEnabled) {
				return false;
			} else */if(!x.isAppoint()) {
				return false;
			} else if(mySecurityService.isAdmin()) {
				return true;
			} else {
				if(x.isOwner(mySecurityService)) {
					return true;
				} else {
					return false;
				}
			}
		}).collect(Collectors.toList());
		
		List<ClientInterviewDate> interviewDateList = candidateService.top5thCandidate(list);
		
		ClientInterviewResponse r = new ClientInterviewResponse();
		r.setList(interviewDateList);
		
		return r;
	}
	
	@PostMapping("/candidate/incomplete")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public InCompleteResponse inComplete() {
		List<Candidate> list = candidateRepository.findAll().stream().filter(x -> {
			/*if(wait for !isEnabled) {
				return false;
			} else */if(!x.isComplete()) {
				return false;
			} else if(mySecurityService.isAdmin()) {
				return true;
			} else {
				if(x.isOwner(mySecurityService)) {
					return true;
				} else {
					return false;
				}
			}
		}).collect(Collectors.toList());
				
		InCompleteResponse r = new InCompleteResponse();
		r.setList(list);
		
		return r;
	}
	
	@PostMapping("/candidate/byid")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public CandidateByIdResponse candidateById(@RequestBody GetById id) {
		Optional<Candidate> cOpt = candidateRepository.findById(id.getId());
		if (cOpt.isPresent()) {
			Candidate c = cOpt.get();
			CandidateByIdResponse r = new CandidateByIdResponse();
			r.setCandidate(c);
			
			return r;
		} else {
			CandidateByIdResponse r = new CandidateByIdResponse();
			r.setCode(-1);
			r.setMessage("Candidate " + id.getId() + " not found.");
			
			return r;
		}
	}
}
