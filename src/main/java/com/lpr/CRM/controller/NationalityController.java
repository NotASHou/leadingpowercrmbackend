package com.lpr.CRM.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Nationality;
import com.lpr.CRM.model.response.AllNationalityResponse;
import com.lpr.CRM.repository.NationalityRepository;

@RestController
public class NationalityController {
	@Autowired
	NationalityRepository nationalityRepository;
	
	@GetMapping("/nationality/all")
	public AllNationalityResponse listNationality() {
		List<Nationality> list = nationalityRepository.findAll().stream().collect(Collectors.toList());
		
		AllNationalityResponse r = new AllNationalityResponse();
		r.setNationalities(list);
		
		return r;
	}
}
