package com.lpr.CRM.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Role;
import com.lpr.CRM.model.response.AllRoleUnderMeResponse;
import com.lpr.CRM.model.response.MyRoleResponse;
import com.lpr.CRM.repository.RoleRepository;
import com.lpr.CRM.service.MySecurityService;

@RestController
public class RoleController {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private MySecurityService mySecurityService;
	
	@PostMapping("/role/allunderme")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
    public AllRoleUnderMeResponse allRoleUnderMe() {
		
		final Role finalMin = mySecurityService.currentRole();
		if(finalMin == null) {
			AllRoleUnderMeResponse response = new AllRoleUnderMeResponse();
			response.setCode(1);
			response.setMessage("denied");
			response.setRoles(new ArrayList<Role>());
			return response; 
		} else {
			List<Role> rs = roleRepository.findAll().stream().filter(r -> (r.getLevel() > finalMin.getLevel())).collect(Collectors.toList());
			AllRoleUnderMeResponse response = new AllRoleUnderMeResponse();
			response.setCode(0);
			response.setMessage("success");
			response.setRoles(rs);
			return response;
		}
    }
	
	@PostMapping("/role/me")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public MyRoleResponse myRole() {
		Role r = mySecurityService.currentRole();
		MyRoleResponse response = new MyRoleResponse();
		response.setRole(r.getName());
		return response;
	}
}
