package com.lpr.CRM.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Candidate;
import com.lpr.CRM.entity.Lead;
import com.lpr.CRM.entity.LeadSource;
import com.lpr.CRM.model.CandidateStatus;
import com.lpr.CRM.model.GeneralResponse;
import com.lpr.CRM.model.MyInterviewDate;
import com.lpr.CRM.model.LeadStatus;
import com.lpr.CRM.model.request.AddedLead;
import com.lpr.CRM.model.request.AddedLeadSource;
import com.lpr.CRM.model.request.DeleteUser;
import com.lpr.CRM.model.request.GetById;
import com.lpr.CRM.model.request.LeadPhone;
import com.lpr.CRM.model.request.UpdatedLead;
import com.lpr.CRM.model.response.LeadByIdResponse;
import com.lpr.CRM.model.response.LeadListResponse;
import com.lpr.CRM.model.response.LeadSourceListResponse;
import com.lpr.CRM.model.response.MyInterviewResponse;
import com.lpr.CRM.repository.CandidateRepository;
import com.lpr.CRM.repository.LeadRepository;
import com.lpr.CRM.repository.LeadSourceRepository;
import com.lpr.CRM.service.LeadService;
import com.lpr.CRM.service.MySecurityService;

@RestController
public class LeadController {
	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	LeadRepository leadRepository;
	@Autowired
	LeadSourceRepository leadSourceRepository;
	@Autowired
	CandidateRepository candidateRepository;
	@Autowired
	MySecurityService mySecurityService;
	@Autowired
	LeadService leadService;
	
	@GetMapping("/lead/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public LeadListResponse leadlist(){
		LeadListResponse response = new LeadListResponse();
		List<Lead> leadlist = leadRepository.findAll().stream().filter(x -> {
			if(x.isEnabled() == false) 
				return false;
			else if(mySecurityService.isAdmin()) {
				return true;
			} else {
				if(leadService.isOwner(x)) {
					return true;
				} else {
					return false;
				}
			}
		}).collect(Collectors.toList());
		response.setLeadlist(leadlist);
		return response;
	}
	
	@GetMapping("/leadsource/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public LeadSourceListResponse leadsourcelist(){
		LeadSourceListResponse response = new LeadSourceListResponse();
		List<LeadSource> leadsourcelist = leadSourceRepository.findAll();
		response.setLeadsourcelist(leadsourcelist);
		return response;
	}

	@PostMapping("/lead/add")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
    public GeneralResponse addLead(@RequestBody AddedLead lead) {
		Lead newlead = new Lead();
		newlead.setName(lead.getName());
		newlead.setSurname(lead.getSurname());
		newlead.setNickname(lead.getNickname());
		newlead.setMobile(lead.getMobile());
		LeadSource source = leadSourceRepository.findById(lead.getLeadSourceId()).get();
		newlead.setSource(source);
		newlead.setAddby(mySecurityService.currentAccount());
		leadRepository.save(newlead);
		
		return new GeneralResponse();
	}
	
	@PostMapping("/leadsource/add")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
    public GeneralResponse addLeadSource(@RequestBody AddedLeadSource leadsource) {
		LeadSource newleadsource = new LeadSource();
		newleadsource.setName(leadsource.getName());
		newleadsource.setCost(leadsource.getCost());
		newleadsource.setCreatedate(new Date());
		
		leadSourceRepository.save(newleadsource);
		
		return new GeneralResponse();
	}
	
	@PostMapping("/lead/phone")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse phone(@RequestBody LeadPhone leadPhone) {
		Lead lead = leadRepository.getOne(leadPhone.getLeadId());
		lead.setStatus(LeadStatus.pass);
		Candidate candidate = leadService.createCandidateForLead(lead);
		candidate.setStatus(CandidateStatus.pass);
		
		candidateRepository.save(candidate);
		leadRepository.save(lead);

		return new GeneralResponse();
	}
		
    @PostMapping("/lead/delete")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin')")
    public GeneralResponse deleteLead(@RequestBody DeleteUser deleteUser) {
    	for(Long id : deleteUser.getIds()) {
    		logger.info("deleteUser with id : " + id);
        	Optional<Lead> leadToDelete = leadRepository.findById(id);
        	if(leadToDelete.isPresent()) {
        		Lead lead = leadToDelete.get();
            	lead.setEnabled(false);
            	leadRepository.save(lead);
        	} else {
        		logger.info("delete id " + id + " not found");
        	}
    	}
    	return new GeneralResponse();
    }
    
    @PostMapping("/lead/update")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
    public GeneralResponse editLead(@RequestBody UpdatedLead updatedLead) {
    	Lead lead = leadRepository.getOne(updatedLead.getId());
    	lead.setName(updatedLead.getName());
    	lead.setSurname(updatedLead.getSurname());
    	lead.setNickname(updatedLead.getNickname());
    	lead.setMobile(updatedLead.getMobile());
    	LeadSource source = leadSourceRepository.getOne(updatedLead.getCurrentSourceId());
    	lead.setSource(source);
    	lead.setStatus(updatedLead.getCurrentStatus());
		lead.setDatetime(updatedLead.getDatetime());
    	leadRepository.save(lead);
    	
    	if(updatedLead.getCurrentStatus() == LeadStatus.pass) {
    		Candidate c = leadService.createCandidateForLead(lead);
    		c.setStatus(CandidateStatus.pass);
    		candidateRepository.save(c);
    	}
    	
    	return new GeneralResponse();
    }
    
    
    @PostMapping("/lead/myInterview")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
    public MyInterviewResponse myInterview() {

    	List<Lead> list = leadRepository.findAll().stream().filter(x -> {
    		if(x.isEnabled() == false) {
				return false;
    		} else if(leadService.leadIsAppoint(x)) {
    			return false;
    		} else if(mySecurityService.isAdmin()) {
				return true;
			} else {
				if(leadService.isOwner(x)) {
					return true;
				} else {
					return false;
				}
			}
    	}).collect(Collectors.toList());
    	
    	List<MyInterviewDate> interviewDateList = leadService.top5thLead(list);
    	
    	MyInterviewResponse r = new MyInterviewResponse();
    	r.setList(interviewDateList);
    	
    	return r;
    }
    
    @PostMapping("/lead/byid")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
    public GeneralResponse byId(@RequestBody GetById id) {
    	Optional<Lead> lOpt = leadRepository.findById(id.getId());
    	if(lOpt.isPresent()) {
    		LeadByIdResponse r = new LeadByIdResponse();
    		r.setLead(lOpt.get());
    		
    		return r;
    	} else {
    		return new GeneralResponse(-1, "Lead id " + id.getId() + " not found.");
    	}
    }
}
