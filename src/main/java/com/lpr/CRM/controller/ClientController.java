package com.lpr.CRM.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lpr.CRM.entity.Branch;
import com.lpr.CRM.entity.Client;
import com.lpr.CRM.entity.Position;
import com.lpr.CRM.entity.Project;
import com.lpr.CRM.entity.lprpackage.LPRPackage;
import com.lpr.CRM.model.GeneralResponse;
import com.lpr.CRM.model.request.AddedBranch;
import com.lpr.CRM.model.request.AddedClient;
import com.lpr.CRM.model.request.AddedPosition;
import com.lpr.CRM.model.request.AddedProject;
import com.lpr.CRM.model.request.UpdatedBranch;
import com.lpr.CRM.model.request.UpdatedClient;
import com.lpr.CRM.model.request.UpdatedPosition;
import com.lpr.CRM.model.request.UpdatedProject;
import com.lpr.CRM.model.response.BranchListResponse;
import com.lpr.CRM.model.response.ClientListResponse;
import com.lpr.CRM.model.response.PositionListResponse;
import com.lpr.CRM.model.response.ProjectListResponse;
import com.lpr.CRM.repository.BranchRepository;
import com.lpr.CRM.repository.ClientRepository;
import com.lpr.CRM.repository.LPRPackageRepository;
import com.lpr.CRM.repository.PositionRepository;
import com.lpr.CRM.repository.ProjectRepository;

@RestController
public class ClientController {

	@Autowired
	ClientRepository clientRepository;
	@Autowired
	BranchRepository branchRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	PositionRepository positionRepository;
	@Autowired
	LPRPackageRepository lprPackageRepository;
	
	@GetMapping("/client/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public ClientListResponse clientlist(){
		List<Client> clients = clientRepository.findAll();
		ClientListResponse response = new ClientListResponse();
		response.setClients(clients);
		return response;
	}
	
	@PostMapping("/client/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse createClient(@RequestBody AddedClient client) {
		Client c = new Client();
		c.setName(client.getName());
		clientRepository.save(c);
		
		return new GeneralResponse();
	}
	
	@GetMapping("/branch/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public BranchListResponse branchlist(@RequestParam Long clientId) {
		List<Branch> branches = clientRepository.findById(clientId).get().getBranches();
		BranchListResponse response = new BranchListResponse();
		response.setBranches(branches);
		return response;
	}
	
	@PostMapping("/branch/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse createBranch(@RequestBody AddedBranch branch) {
		Branch b = new Branch();
		b.setName(branch.getName());
		
		Client c = clientRepository.findById(branch.getClientId()).get();
		c.getBranches().add(b);
		branchRepository.save(b);
		clientRepository.save(c);
		
		return new GeneralResponse();
	}
	
	@GetMapping("/project/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public ProjectListResponse projectlist(@RequestParam Long branchId) {
		List<Project> projects = branchRepository.findById(branchId).get().getProjects();
		ProjectListResponse response = new ProjectListResponse();
		response.setProjects(projects);
		return response;
	}
	
	@PostMapping("/project/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse createProject(@RequestBody AddedProject project) {
		Project p = new Project();
		p.setName(project.getName());
		
		Branch b = branchRepository.findById(project.getBranchId()).get();
		b.getProjects().add(p);
		projectRepository.save(p);
		branchRepository.save(b);
		
		return new GeneralResponse();
	}
	
	@GetMapping("/position/list")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public PositionListResponse positionlist(@RequestParam Long projectId) {
		List<Position> positions = projectRepository.findById(projectId).get().getPositions();
		PositionListResponse response = new PositionListResponse();
		response.setPositions(positions);
		return response;
	}

	@PostMapping("/position/add")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse createPosition(@RequestBody AddedPosition addedPosition) {
		Optional<LPRPackage> packageOpt = lprPackageRepository.findById(addedPosition.getPackageId());
		Optional<Project> projectOpt = projectRepository.findById(addedPosition.getProjectId());
		
		if (!projectOpt.isPresent()) {
			return new GeneralResponse(-1, "Parent project id " + addedPosition.getProjectId() + " not found.");
		} else if (!packageOpt.isPresent()) {
			return new GeneralResponse(-1, "Package id " + addedPosition.getPackageId() + " not found.");
		} else {
			Position newPosition = new Position();
			newPosition.setName(addedPosition.getName());
			newPosition.setLprPackage(packageOpt.get());
			Project project = projectOpt.get();
			project.getPositions().add(newPosition);
			positionRepository.save(newPosition);
			projectRepository.save(project);
			
			return new GeneralResponse();
		}
	}
	
	@PostMapping("/client/delete")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse deleteClient(@RequestBody Long clientId) {
		Client c = clientRepository.findById(clientId).get();
		
		deleteClient(c);
		return new GeneralResponse();
	}
	
	@PostMapping("/branch/delete")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse deleteBranch(@RequestBody Long branchId) {
		Branch b = branchRepository.findById(branchId).get();
		
		deleteBranch(b);
		return new GeneralResponse();
	}

	@PostMapping("/project/delete")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse deleteProject(@RequestBody Long projectId) {
		Project p = projectRepository.findById(projectId).get();
		
		deleteProject(p);
		return new GeneralResponse();
	}

	@PostMapping("/position/delete")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse deletePosition(@RequestBody Long positionId) {
		Position p = positionRepository.findById(positionId).get();
		
		deletePosition(p);
		return new GeneralResponse();
	}
	
	private void deleteClient(Client c) {
		for(Branch branch : c.getBranches()) {
			deleteBranch(branch);
		}
		clientRepository.delete(c);
	}
	
	private void deleteBranch(Branch b) {
		for(Project project : b.getProjects()) {
			deleteProject(project);
		}
		branchRepository.delete(b);
	}
	
	private void deleteProject(Project p) {
		for(Position position : p.getPositions()) {
			deletePosition(position);
		}
		projectRepository.delete(p);
	}
	
	private void deletePosition(Position p) {
		positionRepository.delete(p);
	}
	
	@PostMapping("/client/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse updateClient(@RequestBody UpdatedClient client) {
		Client newClient = clientRepository.findById(client.getId()).get();
		newClient.setName(client.getName());
		
		clientRepository.save(newClient);
		
		return new GeneralResponse();
	}
	
	@PostMapping("/branch/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse updateBranch(@RequestBody UpdatedBranch branch) {
		Branch newBranch = branchRepository.findById(branch.getId()).get();
		newBranch.setName(branch.getName());
		
		branchRepository.save(newBranch);
		
		return new GeneralResponse();
	}

	@PostMapping("/project/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse updateProject(@RequestBody UpdatedProject project) {
		Project newProject = projectRepository.findById(project.getId()).get();
		newProject.setName(project.getName());
		
		projectRepository.save(newProject);
		
		return new GeneralResponse();
	}

	@PostMapping("/position/update")
	@PreAuthorize("hasAuthority('admin') or hasAuthority('superadmin') or hasAuthority('user')")
	public GeneralResponse updatePosition(@RequestBody UpdatedPosition updatedPosition) {
		Optional<LPRPackage> packageOpt = lprPackageRepository.findById(updatedPosition.getPackageId());
		Optional<Position> positionOpt = positionRepository.findById(updatedPosition.getId());
		
		if (!positionOpt.isPresent()) {
			return new GeneralResponse(-1, "Position id " + updatedPosition.getId() + " not found.");
		} else if (!packageOpt.isPresent()) {
			return new GeneralResponse(-1, "Package id " + updatedPosition.getPackageId() + " not found.");
		} else {
			Position position = positionOpt.get();
			position.setName(position.getName());
			position.setLprPackage(packageOpt.get());
			positionRepository.save(position);
			
			return new GeneralResponse();
		}
		
	}

}
